#include "pch.h"
#include "InputHandler.h"

InputHandler::InputHandler(DisplayObject* a)
{
	this->command_l = new CommandToMoveLeft();
	this->owner = a;
}

InputHandler::InputHandler()
{
	
}

void InputHandler::OnUpdate()
{
	DirectX::Keyboard::State keyboard = ResourcesAlejandro::getInstance()->keyboard->GetState();
	ResourcesAlejandro::getInstance()->keyboardTracker.Update(keyboard);

	if (keyboard.L) {
		this->command_l->execute(this->owner);
	}

}
