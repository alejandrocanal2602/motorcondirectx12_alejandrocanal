#pragma once
#include "DisplayObject.h"
#include <math.h>

class DisplayObject;

class Transform
{
public:

	//Contructores - Destructor
	Transform();
	Transform(DisplayObject* _parent);
	~Transform();

	DirectX::XMMATRIX translate;
	DirectX::XMMATRIX rotate;
	DirectX::XMMATRIX scale;
	DirectX::XMMATRIX transformation;
	DirectX::XMMATRIX acumulate;


	DisplayObject* ownerDO;

	void Translate();
	void Scale();
	void Rotate();
	void Update();
};

