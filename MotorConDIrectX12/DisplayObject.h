#pragma once
#include "Transform.h"
#include "Vector3.h"

class Transform;
class DisplayObject
{
public:

	//Contructores-Destructor
	DisplayObject(DisplayObject* _parent);
	DisplayObject();
	~DisplayObject();

	virtual void OnDraw();
	virtual void OnUpdate();
	virtual void OnKeyboardInput(unsigned char key, int x, int y);

	//Pedir Id
	unsigned int GetId();

	Vector3 position;
	Vector3 scale;
	Vector3 rotation;
	

	Transform* transform;
	DisplayObject* parent;

private:
	unsigned int id;

};

