#include "pch.h"
#include "Vector3.h"

Vector3::Vector3(float _x, float _y, float _z)
{
	x = _x;
	y = _y;
	z = _z;
}

Vector3* Vector3::operator+(Vector3* vec)
{
	return new Vector3(x + vec->x, y + vec->y, z + vec->z);
}

Vector3* Vector3::operator-(Vector3* vec)
{
	return new Vector3(x + vec->x, y + vec->y, z + vec->z);
}

Vector3* Vector3::CrossProduct(Vector3* vec)
{

	return  Vector3((y * vec->z - z * vec->y), (z * vec->x - x * vec->z), (x * vec->y - y * vec->x)).Normalizado();
}

float Vector3::Dot(Vector3* vec)
{
	return x * vec->x + y * vec->y;
}

Vector3 Vector3::operator*(float a)
{
	return Vector3(x * a, y * a, z * a);
}

Vector3 Vector3::operator/(float a)
{
	return Vector3(x / a, y / a, z / a);
}

Vector3* Vector3::Normalizado()
{
	return new Vector3(x / Modulo(), y / Modulo(), z / Modulo());
}

float Vector3::Modulo()
{
	return sqrt(x * x + y * y + z * z);
}

void Vector3::Normalizar()
{
	x = x / Modulo();
	y = y / Modulo();
	z = z / Modulo();
}

Vector3::Vector3()
{
}


Vector3::~Vector3()
{
}

