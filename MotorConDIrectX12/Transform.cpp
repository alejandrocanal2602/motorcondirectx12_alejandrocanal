#include "pch.h"
#include "Transform.h"

Transform::Transform() 
{
	ownerDO = NULL;
}

Transform::Transform(DisplayObject* _parent)
{
	ownerDO = _parent;
}


Transform::~Transform()
{
}


void Transform::Translate()
{
	translate = DirectX::XMMatrixIdentity();
	translate = DirectX::XMMatrixTranslation(ownerDO->position.x, ownerDO->position.y, ownerDO->position.z);
}

void Transform::Scale()
{
	scale = DirectX::XMMatrixIdentity();
	scale = DirectX::XMMatrixScaling(ownerDO->scale.x,ownerDO->scale.y,ownerDO->scale.z);
}


void Transform::Rotate()
{
	rotate = DirectX::XMMatrixIdentity();
	DirectX::XMMATRIX x = DirectX::XMMatrixRotationX(ownerDO->rotation.x);
	DirectX::XMMATRIX y = DirectX::XMMatrixRotationY(ownerDO->rotation.y);
	DirectX::XMMATRIX z = DirectX::XMMatrixRotationZ(ownerDO->rotation.z);

	rotate = x * y * z;

}

void Transform::Update()
{

	rotate = DirectX::XMMatrixIdentity();
	scale = DirectX::XMMatrixIdentity();
	translate = DirectX::XMMatrixIdentity();

	Rotate();
	Scale();
	Translate();

	transformation = rotate * scale * translate;
	


	if (ownerDO->parent != nullptr)
	{
		this->acumulate = ownerDO->parent->transform->acumulate * this->transformation;
	}
	else
	{
		acumulate = transformation;
		//acumulate = DirectX::XMMatrixIdentity();
	}
}




