#pragma once
#include<unordered_map>
#include <string>
#include <WaveFrontReader.h>
#include "Keyboard.h"

class ResourcesAlejandro
{
public:
	static ResourcesAlejandro* getInstance();

	WaveFrontReader<uint16_t> GetMesh(std::string key);
	std::unordered_map<std::string, std::shared_ptr<WaveFrontReader<uint16_t>>> m_pool;


	std::unique_ptr<DirectX::Keyboard> keyboard;
	DirectX::Keyboard::KeyboardStateTracker keyboardTracker;


	//mesh = std::make_unique<WaveFrontReader<uint16_t>>();
	//mesh->Load(L"Assets\\c.obj", false);

private:
	static ResourcesAlejandro* instance;
	ResourcesAlejandro();
};


