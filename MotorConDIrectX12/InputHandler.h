#pragma once
#include "Command.h"
#include "CommandToMoveLeft.h"
#include "DisplayObject.h"
#include "Keyboard.h"
#include "Resources.h"



class InputHandler
{
public:
	InputHandler(DisplayObject* a);
	InputHandler();
	void OnUpdate();
	DisplayObject* owner;

private:
	CommandToMoveLeft* command_l;
};