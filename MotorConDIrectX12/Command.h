#pragma once
#include "DisplayObject.h"

class Command
{
public:
	virtual ~Command() {}
	virtual void execute(DisplayObject* a) = 0;
};



