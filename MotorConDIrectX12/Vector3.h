#pragma once
#include<math.h>

class Vector3
{
public:
	float x = 0;
	float y = 0;
	float z = 0;

	Vector3* operator+(Vector3* vec);
	Vector3* operator-(Vector3* vec);
	Vector3* CrossProduct(Vector3* vec);
	Vector3 operator*(float a);
	Vector3 operator/(float a);
	Vector3* Normalizado();

	float Dot(Vector3* vec);
	void Normalizar();
	float Modulo();
	Vector3(float _x, float _y, float _z);
	Vector3();
	~Vector3();
};

