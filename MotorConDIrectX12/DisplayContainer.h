#pragma once
#include<vector>
#include "DisplayObject.h"
class DisplayContainer : public DisplayObject
{
public:
	DisplayContainer();
	~DisplayContainer();

	void OnUpdate() override;
	void OnKeyboardInput(unsigned char key, int x, int y) override;
	void OnDraw() override;

	std::pair<bool, int> findChild(DisplayObject* d);

	int addChild(DisplayObject* d);
	int removeChildIndex(int index);

	DisplayObject* getChildAt(int index);
	DisplayObject* removeChild(int index);

private:
	std::vector<DisplayObject*> children;
};

