static float3  albedo = float3(1.0,0,0);     // value=1,0,0
static float metallic = 0.13;   // value=0, min=0, max=1, step=0.001
static float roughness = 0.33f;


static const float PI = 3.14159265359;

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
	float3 normal : NORMAL;
	float3 view: TEXCOORD0;
	float3 vertex: TEXCOORD1;
};


float random(float2 st)
{
	return frac(sin(dot(st, float2(12.9898, 78.233))) * 43758.5453123);
}

float3 my_texture(float2 st, float tilling)
{
	st *= tilling;
	float x = step(0.5, frac(st.x));
	float y = step(0.5, frac(st.y));
	float c = 0.0f;

	if (x != y)
		c = 1.0;
	return c;
}

float noise_texture(float2 uv)
{
	uv *= 10.0f;
	float2 id = floor(uv);
	float2 f = frac(uv);

	float a = random(id);//esquina inferior izquierda
	float b = random(id + float2(1.0f, 0.0f));
	float c = random(id + float2(0.0f, 1.0f));
	float d = random(id + float2(1.0f, 1.0f));

	float2 u = f * f * (3.0f - 2.0f * f);

	return lerp(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}

float fbm(float2 st)
{
	float f = noise_texture(st) * 0.5;
	f += noise_texture(st * 2.0) * 0.25;
	f += noise_texture(st * 4.0) * 0.125;
	f += noise_texture(st * 8.0) * 0.0625;
	return f;
}

float distributionGGX(float3 N, float3 H, float roughness) {
	float a2 = roughness * roughness * roughness * roughness;
	float NdotH = max(dot(N, H), 0.0);
	float denom = (NdotH * NdotH * (a2 - 1.0) + 1.0);
	return a2 / (PI * denom * denom);
}

float geometrySchlickGGX(float NdotV, float roughness) {
	float r = (roughness + 1.0);
	float k = (r * r) / 8.0;
	return NdotV / (NdotV * (1.0 - k) + k);
}

float geometrySmith(float3 N, float3 V, float3 L, float roughness) {
	return geometrySchlickGGX(max(dot(N, L), 0.0), roughness) *
		geometrySchlickGGX(max(dot(N, V), 0.0), roughness);
}

float3 fresnelSchlick(float cosTheta, float3 F0) {
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

float4 main(PixelShaderInput input) : SV_TARGET
{

	float3 lightPos = float3(0, 1.0, 1);
	float3 lightColor = float3(1.0,1,1);
	//float3 totColor = float3(1.0f,1.0f,1.0f);

	float3 worldPos = input.vertex;
	float3 N = normalize(input.normal);
	float3 V = normalize(input.vertex - input.view);
	float3 L = normalize(lightPos - input.normal);
	float3 H = normalize(V + L);

	float3  F0 = lerp(float3(0.04, 0.04, 0.04), pow(albedo, float3(2.2, 2.2, 2.2)), metallic);
	float NDF = distributionGGX(N, H, roughness);
	float G = geometrySmith(N, V, L, roughness);
	float3  F = fresnelSchlick(max(dot(H, V), 0.0), F0);
	float3  kD = float3(1.0, 1.0, 1.0) - F;
	kD *= 1.0 - metallic;

	float3  numerator = NDF * G * F;
	float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
	float3  specular = numerator / max(denominator, 0.001);

	float NdotL = max(dot(N, L), 0.0);
	float3  color = lightColor * (kD * pow(albedo, float3(2.2, 2.2, 2.2)) / PI + specular) *
		(NdotL / dot(lightPos - worldPos, lightPos - worldPos));

	float4 fragColor = float4(pow(color / (color + 1.0), float3((1.0 / 2.2), (1.0 / 2.2), (1.0 / 2.2))), 1.0);

	//roughness = fbm(input.view * 1000);
	//metallic = fbm(input.view * 500);

	return fragColor;
}
