struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
	float3 normal : NORMAL;
	float3 view: TEXCOORD0;
	float3 vertex: TEXCOORD1;
};

float3 lights(PixelShaderInput input) {

	float3 L = normalize(float3(1.0, -1.0, -1.0));

	float I = 0.0f;
	float Ia = 0.22f;

	//kd*max(dot(-l,normal),0);
	float Id = 1.0f * max(dot(-L, input.normal), 0.0f);

	float3 V = normalize(input.vertex - input.view);
	float3 H = -L + V;
	float power = 20;
	float R = reflect(L, normalize(input.normal));

	float Is = 0.1f * max(pow(dot(R, V), power), 0);

	float power2 = 200.0f;

	I = Ia + Id + Is;

	return I;
}


float4 main(PixelShaderInput input) : SV_TARGET
{
	float3 col = lights(input);
	return float4(col, 1.0f);
}
