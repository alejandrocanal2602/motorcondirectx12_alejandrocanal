cbuffer ModelViewProjectionConstantBuffer : register(b0)
{
	matrix model;
	matrix view;
	matrix projection;
};

struct VertexShaderInput
{
	float3 pos : POSITION;
	float3 color : COLOR0;
	float3 normal : NORMAL;
	float2 texcoord: TEXCOORD0;
};

struct PixelShaderInput
{
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
	float3 normal : NORMAL;
	float3 view: TEXCOORD0;
	float3 vertex: TEXCOORD1;
};


PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;
	float4 pos = float4(input.pos, 1.0f);

	pos = mul(pos, model);
	output.vertex = pos;
	pos = mul(pos, view);
	pos = mul(pos, projection);
	output.pos = pos;

	float4 norm = float4(input.normal, 1.0f);
	norm = mul(norm, model);

	output.normal = normalize(norm);
	output.color = input.color; //input.color;
	output.view = view[3].xyz;

	return output;
}
