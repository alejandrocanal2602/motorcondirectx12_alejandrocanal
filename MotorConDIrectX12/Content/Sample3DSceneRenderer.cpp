﻿#include "pch.h"
//
//#include "Sample3DSceneRenderer.h"
//
//#include "..\Common\DirectXHelper.h"
//#include <ppltasks.h>
//#include <synchapi.h>
//
//using namespace MotorConDIrectX12;
//
//using namespace Concurrency;
//using namespace DirectX;
//using namespace Microsoft::WRL;
//using namespace Windows::Foundation;
//using namespace Windows::Storage;
//
//// Índices al mapa de estado de la aplicación.
//Platform::String^ AngleKey = "Angle";
//Platform::String^ TrackingKey = "Tracking";
//
//// Carga los sombreadores de vértices y píxeles de los archivos y crea instancias de la geometría de cubo.
//Sample3DSceneRenderer::Sample3DSceneRenderer(const std::shared_ptr<DX::DeviceResources>& deviceResources) :
//	m_radiansPerSecond(XM_PIDIV4),	// girar 45 grados por segundo
//	m_angle(0),
//	m_tracking(false),
//	m_deviceResources(deviceResources)
//{
//	manzana = new RenderObject(deviceResources);
//	/*manzana->position.x = 1;
//	manzana->position.y = 1;
//	manzana->position.z = 1;
//
//	manzana->scale.x = 1;
//	manzana->scale.y = 1;
//	manzana->scale.z = 1;
//
//	manzana->rotation.x = 0;
//	manzana->rotation.y = 0;
//	manzana->rotation.z = 0;*/
//
//	//LoadState();
//
//	CreateDeviceDependentResources();
//	CreateWindowSizeDependentResources();
//}
//
//Sample3DSceneRenderer::~Sample3DSceneRenderer()
//{
//	manzana->~RenderObject();
//}
//
//void Sample3DSceneRenderer::CreateDeviceDependentResources()
//{
//	manzana->CreateDeviceDependentResources();
//}
//
//// Inicializa los parámetros de vista cuando cambia el tamaño de la ventana.
//void Sample3DSceneRenderer::CreateWindowSizeDependentResources()
//{
//	manzana->CreateWindowSizeDependentResources();
//}
//
//// Se llama una vez por fotograma, gira el cubo y calcula las matrices de modelo y vista.
//void Sample3DSceneRenderer::Update(DX::StepTimer const& timer)
//{
//	manzana->OnUpdate();
//}
//
//// Guarda el estado actual del representador.
//void Sample3DSceneRenderer::SaveState()
//{
//	auto state = ApplicationData::Current->LocalSettings->Values;
//
//	if (state->HasKey(AngleKey))
//	{
//		state->Remove(AngleKey);
//	}
//	if (state->HasKey(TrackingKey))
//	{
//		state->Remove(TrackingKey);
//	}
//
//	state->Insert(AngleKey, PropertyValue::CreateSingle(m_angle));
//	state->Insert(TrackingKey, PropertyValue::CreateBoolean(m_tracking));
//}
//
//// Restaura el estado anterior del representador.
//void Sample3DSceneRenderer::LoadState()
//{
//	auto state = ApplicationData::Current->LocalSettings->Values;
//	if (state->HasKey(AngleKey))
//	{
//		m_angle = safe_cast<IPropertyValue^>(state->Lookup(AngleKey))->GetSingle();
//		state->Remove(AngleKey);
//	}
//	if (state->HasKey(TrackingKey))
//	{
//		m_tracking = safe_cast<IPropertyValue^>(state->Lookup(TrackingKey))->GetBoolean();
//		state->Remove(TrackingKey);
//	}
//}
//
//// Gire el modelo de cubo 3D una cantidad de radianes establecida.
//void Sample3DSceneRenderer::Rotate(float radians)
//{
//	// Prepárese para pasar al sombreador la matriz de modelo actualizada
//	//XMStoreFloat4x4(&m_constantBufferData.model, XMMatrixTranspose(XMMatrixRotationY(radians)));
//}
//
//void Sample3DSceneRenderer::StartTracking()
//{
//	m_tracking = true;
//}
//
//// Al realizar un seguimiento, el cubo 3D puede girarse alrededor de su eje Y siguiendo la posición del puntero en relación con el ancho de la pantalla de salida.
//void Sample3DSceneRenderer::TrackingUpdate(float positionX)
//{
//	if (m_tracking)
//	{
//		//float radians = XM_2PI * 2.0f * positionX / m_deviceResources->GetOutputSize().Width;
//		//Rotate(radians);
//	}
//}
//
//void Sample3DSceneRenderer::StopTracking()
//{
//	m_tracking = false;
//}
//
//// Presenta un fotograma usando los sombreadores de vértices y píxeles.
//bool Sample3DSceneRenderer::Render()
//{
//	//Render
//	return manzana->Render();
//}
//
//
