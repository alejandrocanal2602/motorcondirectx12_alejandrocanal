#include "pch.h"
#include "Resources.h"
ResourcesAlejandro* ResourcesAlejandro::instance = 0;

ResourcesAlejandro::ResourcesAlejandro()
{
	this->keyboard = std::make_unique<DirectX::Keyboard>();
}

ResourcesAlejandro* ResourcesAlejandro::getInstance()
{
	if (instance == 0)
	{
		instance = new ResourcesAlejandro();
	}
	return instance;
}


//L"Assets\\c.obj"
WaveFrontReader<uint16_t> ResourcesAlejandro::GetMesh(std::string key)
{
	if (this->m_pool.count(key) < 1)
	{
		std::wstring widestrKey = std::wstring(key.begin(), key.end());
		const wchar_t* widecstrKey = widestrKey.c_str();
		this->m_pool[key] = std::make_shared<WaveFrontReader<uint16_t>>();
		this->m_pool[key]->Load(widecstrKey, false);
	}

	//return this->m_pool[key];
	
	return  WaveFrontReader<uint16_t>();
}





