#pragma once
#include "Command.h"
#include "DisplayObject.h"


class RotateToLeft : public Command
{
public:
	RotateToLeft();
	virtual void execute(DisplayObject* obj);
};

