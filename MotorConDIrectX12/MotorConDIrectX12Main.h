﻿/*
#pragma once

#include "Common\StepTimer.h"
#include "Common\DeviceResources.h"
#include "Content\Sample3DSceneRenderer.h"
#include "RenderObject.h"

// Representa contenido Direct3D en la pantalla.
namespace MotorConDIrectX12
{
	class MotorConDIrectX12Main
	{
	public:
		MotorConDIrectX12Main();
		void CreateRenderers(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Update();
		bool Render();

		void OnWindowSizeChanged();
		void OnSuspending();
		void OnResuming();
		void OnDeviceRemoved();

	private:
		// TODO: Sustituir con sus propios representadores de contenido.
		std::unique_ptr<Sample3DSceneRenderer> m_sceneRenderer;

		// Temporizador de bucle de representación.
		DX::StepTimer m_timer;
	};
}

*/


#pragma once
#include "Common\StepTimer.h"
#include "Common\DeviceResources.h"
#include "Content\Sample3DSceneRenderer.h"
#include "RenderObject.h"

// Representa contenido Direct3D en la pantalla.
namespace MotorConDIrectX12
{
	class MotorConDIrectX12Main
	{
	public:
		MotorConDIrectX12Main();
		void CreateRenderers(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		void Update();
		bool Render();

		void OnWindowSizeChanged();
		void OnSuspending();
		void OnResuming();
		void OnDeviceRemoved();

	private:
		// TODO: Sustituir con sus propios representadores de contenido.
		std::unique_ptr<RenderObject> m_sceneRenderer;

		// Temporizador de bucle de representación.
		DX::StepTimer m_timer;
	};
}

