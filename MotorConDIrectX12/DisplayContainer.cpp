#include "pch.h"
#include "DisplayContainer.h"

std::pair<bool, int> DisplayContainer::findChild(DisplayObject* d)
{
	std::pair<bool, int> result;

	std::vector<DisplayObject*>::iterator it = std::find(children.begin(), children.end(), d);

	if (it != children.end())
	{
		result.first = true;
		result.second = std::distance(children.begin(), it);
	}
	else
	{
		result.first = false;
		result.second = -1;
	}

	return result;
}



DisplayObject* DisplayContainer::getChildAt(int index)
{
	DisplayObject* aux = children.at(index);
	if (findChild(aux).first == false)
	{
		return nullptr;
	}

	return aux;

}

int DisplayContainer::addChild(DisplayObject* d)
{
	if (findChild(d).first == false)
	{
		children.push_back(d);
		d->parent = this;
	}

	return findChild(d).second;
}



void DisplayContainer::OnUpdate()
{
	DisplayObject::OnUpdate();

	for (size_t i = 0; i < children.size(); i++)
	{
		children[i]->OnUpdate();
	}
}

void DisplayContainer::OnKeyboardInput(unsigned char key, int x, int y)
{
	DisplayObject::OnKeyboardInput(key, x, y);

	for (int i = 0; i < children.size(); i++)
	{
		children[i]->OnKeyboardInput(key, x, y);
	}
}


void DisplayContainer::OnDraw()
{
	DisplayObject::OnDraw();

	for (size_t i = 0; i < children.size(); i++)
	{
		children[i]->OnDraw();
	}
}

int DisplayContainer::removeChildIndex(int index)
{
	if (findChild(children.at(index)).first == false)
	{
		return NULL;
	}

	int aux = findChild(children.at(index)).second;

	children.erase(std::find(children.begin(), children.end(), children.at(index)));

	return aux;
}

DisplayObject* DisplayContainer::removeChild(int index)
{

	if (findChild(children.at(index)).first == false)
	{
		return nullptr;
	}

	DisplayObject* aux = new DisplayObject();

	aux = children.at(index);

	children.erase(std::find(children.begin(), children.end(), children.at(index)));

	return aux;
}


DisplayContainer::DisplayContainer()
{

}


DisplayContainer::~DisplayContainer()
{
}

