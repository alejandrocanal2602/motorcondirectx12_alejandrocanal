#pragma once
#include "Command.h"
#include "DisplayObject.h"


class CommandToMoveLeft : public Command
{
public:
	CommandToMoveLeft();
	virtual void execute(DisplayObject* obj);
};

