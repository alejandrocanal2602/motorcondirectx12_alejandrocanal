#include "pch.h"
#include "DisplayObject.h"

DisplayObject::DisplayObject(DisplayObject* _parent)
{
	parent = _parent;
	position.x = 0;
	position.y = 0;
	position.z = 0;
	rotation.x = 0;
	rotation.y = 0;
	rotation.z = 0;
	scale.x = 1;
	scale.y = 1;
	scale.z = 1;
	transform = new Transform(this);
}

DisplayObject::DisplayObject()
{
	parent = nullptr;
	position.x = 0;
	position.y = 0;
	position.z = 0;
	rotation.x = 0;
	rotation.y = 0;
	rotation.z = 0;
	scale.x = 1;
	scale.y = 1;
	scale.z = 1;
	transform = new Transform(this);
}

DisplayObject::~DisplayObject()
{
}

unsigned int DisplayObject::GetId()
{
	return id;
}

void DisplayObject::OnDraw()
{

}

void DisplayObject::OnUpdate()
{
	this->transform->Update();
}

void DisplayObject::OnKeyboardInput(unsigned char key, int x, int y)
{

}



