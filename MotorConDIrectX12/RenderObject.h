/*
#pragma once

#include "DisplayContainer.h"
#include "..\Common\DeviceResources.h"
#include "..\Content\ShaderStructures.h"
#include "..\Common\StepTimer.h"
#include <WaveFrontReader.h>
#include "Resources.h"


namespace MotorConDIrectX12
{
	// Este representador de ejemplo crea una instancia de una canalizaci�n de representaci�n b�sica.
	class RenderObject : public DisplayContainer
	{
	public:
		RenderObject(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		~RenderObject();
		void CreateDeviceDependentResources();
		void CreateWindowSizeDependentResources();
		//void Update(DX::StepTimer const& timer);
		virtual void OnUpdate() override;
		bool Render();

	private:
		// Los b�feres de constantes deben tener una alineaci�n de 256 bytes.
		static const UINT c_alignedConstantBufferSize = (sizeof(ModelViewProjectionConstantBuffer) + 255) & ~255;

		// Puntero almacenado en cach� para los recursos del dispositivo.
		std::shared_ptr<DX::DeviceResources> m_deviceResources;

		// Recursos de Direct3D para la geometr�a de cubo.
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>	m_commandList;
		Microsoft::WRL::ComPtr<ID3D12RootSignature>			m_rootSignature;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_pipelineState;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		m_cbvHeap;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_vertexBuffer;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_indexBuffer;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_constantBuffer;
		ModelViewProjectionConstantBuffer					m_constantBufferData;
		UINT8*												m_mappedConstantBuffer;
		UINT												m_cbvDescriptorSize;
		D3D12_RECT											m_scissorRect;
		std::vector<byte>									m_vertexShader;
		std::vector<byte>									m_pixelShader;
		D3D12_VERTEX_BUFFER_VIEW							m_vertexBufferView;
		D3D12_INDEX_BUFFER_VIEW								m_indexBufferView;

		// Variables usadas con el bucle de representaci�n.
		bool	m_loadingComplete;
		float	m_radiansPerSecond;
		float	m_angle;
		bool	m_tracking;
		unsigned int index_num;

		std::unique_ptr<WaveFrontReader<uint16_t>> mesh;
	};
}
*/

#pragma once

#include "DisplayContainer.h"
#include "..\Common\DeviceResources.h"
#include "..\Content\ShaderStructures.h"
#include "..\Common\StepTimer.h"
#include <WaveFrontReader.h>
#include <Resources.h>
#include "InputHandler.h"


namespace MotorConDIrectX12
{
	// Este representador de ejemplo crea una instancia de una canalizacin de representacin bsica.
	class RenderObject : public DisplayObject
	{
	public:
		RenderObject(const std::shared_ptr<DX::DeviceResources>& deviceResources);
		~RenderObject();
		void CreateDeviceDependentResources();
		void CreateWindowSizeDependentResources();
		void Update(DX::StepTimer const& timer);
		bool Render();
		void SaveState();

		void StartTracking();
		void TrackingUpdate(float positionX);
		void StopTracking();
		bool IsTracking() { return m_tracking; }



	private:
		void LoadState();
		void Rotate(float radians);

	private:
		// Los bferes de constantes deben tener una alineacin de 256 bytes.
		static const UINT c_alignedConstantBufferSize = (sizeof(ModelViewProjectionConstantBuffer) + 255) & ~255;

		// Puntero almacenado en cach para los recursos del dispositivo.
		std::shared_ptr<DX::DeviceResources> m_deviceResources;

		// Recursos de Direct3D para la geometra de cubo.
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>	m_commandList;
		Microsoft::WRL::ComPtr<ID3D12RootSignature>			m_rootSignature;
		Microsoft::WRL::ComPtr<ID3D12PipelineState>			m_pipelineState;
		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap>		m_cbvHeap;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_vertexBuffer;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_indexBuffer;
		Microsoft::WRL::ComPtr<ID3D12Resource>				m_constantBuffer;
		ModelViewProjectionConstantBuffer					m_constantBufferData;
		UINT8* m_mappedConstantBuffer;
		UINT												m_cbvDescriptorSize;
		D3D12_RECT											m_scissorRect;
		std::vector<byte>									m_vertexShader;
		std::vector<byte>									m_pixelShader;
		D3D12_VERTEX_BUFFER_VIEW							m_vertexBufferView;
		D3D12_INDEX_BUFFER_VIEW								m_indexBufferView;

		// Variables usadas con el bucle de representacin.
		bool	m_loadingComplete;
		float	m_radiansPerSecond;
		float	m_angle;
		bool	m_tracking;
		unsigned int index_num;
		InputHandler* inputH;

		std::unique_ptr<WaveFrontReader<uint16_t>> mesh;
	};
}

